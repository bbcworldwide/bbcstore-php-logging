<?php

namespace BBCStore\Logging\Tests;

use BBCStore\Logging\Formatter\JsonFormatter;
use BBCStore\Logging\LoggerFactory;
use Monolog\Handler\StreamHandler;
use Zend\Diactoros\Request;
use Monolog\Logger;
use BBCStore\Logging\Processor\CorrelationProcessor;

class LoggerFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testGetInstance()
    {
        $request = new Request();
        $instance = LoggerFactory::getInstance('example', 'info', $request);

        self::assertInstanceOf(Logger::class, $instance);
        self::assertInstanceOf(StreamHandler::class, $instance->getHandlers()[0]);
        self::assertInstanceOf(CorrelationProcessor::class, $instance->getProcessors()[0]);
        self::assertInstanceOf(JsonFormatter::class, $instance->getHandlers()[0]->getFormatter());
    }
}
