<?php

namespace BBCStore\Logging\Tests;

use BBCStore\Logging\Middleware\RequestLogger;
use Zend\Diactoros\Request;
use Psr\Log\LoggerInterface;
use Zend\Diactoros\Response;
use Zend\Diactoros\Uri;

class RequestLoggerTest extends \PHPUnit_Framework_TestCase
{
    public function testSuccess()
    {
        $request = new Request();
        $uri = new Uri('http://www.example.com/foo?bar=baz');
        $request = $request
            ->withUri($uri)
            ->withMethod('GET')
            ->withHeader('content-type', 'application/xml');

        $response = new Response();

        $next = function ($request, $response) {
            return $response
                ->withStatus(200)
                ->withHeader('content-type', 'application/json');
        };

        $message = null;

        $logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $logger->expects($this->once())
            ->method('info')
            ->withAnyParameters()
            ->willReturnCallback(function ($message, $context) {
                self::assertEquals('OK', $message);
                self::assertEquals('inbound-service-call', $context['type']);
                self::assertGreaterThan('0', $context['duration']);
                // Request
                self::assertEquals('GET', $context['request']['method']);
                self::assertEquals('http', $context['request']['scheme']);
                self::assertEquals('/foo', $context['request']['path']);
                self::assertEquals(['bar' => 'baz'], $context['request']['query']);
                self::assertEquals(['application/xml'], $context['request']['headers']['content-type']);
                // Response

                self::assertEquals(200, $context['response']['status']);
                self::assertEquals(['application/json'], $context['response']['headers']['content-type']);
            });

        $middleware = new RequestLogger($logger);
        $middleware($request, $response, $next);
    }

    public function testClientError()
    {
        $request = new Request();
        $response = new Response();

        $next = function ($request, $response) {
            return $response->withStatus(401);
        };

        $message = null;

        $logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $logger->expects($this->once())
            ->method('warning')
            ->withAnyParameters()
            ->willReturnCallback(function ($message, $context) {
                self::assertEquals('Unauthorized', $message);
                self::assertEquals(401, $context['response']['status']);
            });

        $middleware = new RequestLogger($logger);
        $middleware($request, $response, $next);
    }

    public function testServerError()
    {
        $request = new Request();
        $response = new Response();

        $next = function ($request, $response) {
            return $response->withStatus(503);
        };

        $message = null;

        $logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $logger->expects($this->once())
            ->method('critical')
            ->withAnyParameters()
            ->willReturnCallback(function ($message, $context) {
                self::assertEquals('Service Unavailable', $message);
                self::assertEquals(503, $context['response']['status']);
            });

        $middleware = new RequestLogger($logger);
        $middleware($request, $response, $next);
    }
}
