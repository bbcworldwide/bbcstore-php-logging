<?php

namespace BBCStore\Logging;

use BBCStore\Logging\Formatter\JsonFormatter;
use BBCStore\Logging\Processor\CorrelationProcessor as Processor;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\RequestInterface as Request;

/**
 * Logger Factory
 *
 * Provides an instance of Monolog configured with appropriate formatter, processor and handler.
 */
class LoggerFactory
{
    /**
     * Get instance.
     *
     * @param string $name                         Service name
     * @param string $level                        Log level
     * @param RequestInterface|null $request       Request object to provide correlator info
     * @param CorrelationProcessor|null $processor Correlation processor to create correlator info
     *
     * @return \Monolog\Logger                     Logger instance
     */
    public static function getInstance($name, $level, Request $request = null, Processor $processor = null)
    {
        $logger = new \Monolog\Logger($name);

        if ($processor === null) {
            $processor = ($request) ? new Processor($request) : new Processor();
        }

        $handler = new StreamHandler('php://stdout', $level);
        $handler->setFormatter(new JsonFormatter());

        $logger->pushProcessor($processor);
        $logger->pushHandler($handler);

        return $logger;
    }
}
