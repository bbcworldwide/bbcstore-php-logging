<?php

namespace BBCStore\Logging\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Logs HTTP requests/responses.
 */
class RequestLogger
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var array HTTP code to message mappings.
     */
    protected $httpCodes = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
    ];

    /**
     * Public constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param callable          $next
     *
     * @return ResponseInterface
     */
    public function __invoke(RequestInterface $request, ResponseInterface $response, $next)
    {
        $start = microtime(true);

        /** @var ResponseInterface $response */
        $response = $next($request, $response);

        $now    = microtime(true);
        $status = $response->getStatusCode();

        if ($status < 400) {
            $method = 'info';
        } elseif ($status < 500) {
            $method = 'warning';
        } else {
            $method = 'critical';
        }

        parse_str($request->getUri()->getQuery(), $query);

        $context = [
            'type'     => 'inbound-service-call',
            'duration' => $now - $start,
            'request'  => [
                'method'   => $request->getMethod(),
                'scheme'   => $request->getUri()->getScheme(),
                'hostname' => $request->getUri()->getHost(),
                'path'     => $request->getUri()->getPath(),
                'query'    => $query,
                'headers'  => $request->getHeaders(),
            ],
            'response' => [
                'status'  => $status,
                'headers' => $response->getHeaders(),
            ],
        ];

        // In case of error, gather request and response body and add to log context
        // Not using body->getContents as that returns `the remaining contents` of the stream, not the whole thing
        if ($status >= 400) {
            foreach (['request' => $request, 'response' => $response] as $key => $value) {
                /** @var RequestInterface|ResponseInterface $value */
                $value->getBody()->rewind();
                $context[$key]['body'] = $value->getBody()->getContents();
            }
        }

        $this->logger->$method($this->httpCodes[$status], $context);

        return $response;
    }
}
